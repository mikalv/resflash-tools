#!/bin/sh

# Host-specific etc configure for rtest
# Copyright Brian Conway <bconway@rcesoftware.com>, see LICENSE for details

echo 'dhcp' > ${MNTPATH}/fs/etc/hostname.em0
echo 'inet6 autoconf' >> ${MNTPATH}/fs/etc/hostname.em0
chmod 640 ${MNTPATH}/fs/etc/hostname.em0
