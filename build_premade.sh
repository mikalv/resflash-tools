#!/bin/sh

# Build premade resflash images from -stable sets
# Copyright Brian Conway <bconway@rcesoftware.com>, see LICENSE for details
# prerequisite: sets in /usr/rel
# usage: su root -c './build_premade.sh /home/bconway/resflash'

set -o errexit
set -o nounset
if set -o|fgrep -q pipefail; then
  set -o pipefail
fi

if [ ${#} -ne 1 ]; then
  echo "Usage: ${0} resflash_dir"
  exit 1
else
  RESFLASHBIN=${1}
fi

RELDIR=/usr/rel
SETS='base man'
DESTDIR=/usr/rdest
MACHINE=$(machine)

case ${MACHINE} in
  amd64) KERNEL=bsd.mp
         KERNELCP=bsd.rd
         COM0=115200;;
  i386) KERNEL=bsd
        KERNELCP='bsd.mp bsd.rd'
        COM0=38400;;
  *) echo 'Unsupported arch.'
     exit 1;;
esac

# Clean old images

echo '----- Cleaning old images -----'
rm -rf resflash-*.{fs,img}* premade ${DESTDIR}
sync

# Make base dir

echo '----- Making base dir -----'
mkdir ${DESTDIR}
if [ -n "${KERNEL}" ]; then
  cp ${RELDIR}/${KERNEL} ${DESTDIR}/bsd
fi
for kern in ${KERNELCP}; do
  cp ${RELDIR}/${kern} ${DESTDIR}
done
for set in ${SETS}; do
  tar zxfph ${RELDIR}/${set}??.tgz -C ${DESTDIR}
done
# Add on non-set
tar zxfph ${DESTDIR}/var/sysmerge/etc.tgz -C ${DESTDIR}

# Build images
mkdir -p premade/{install,upgrade}

compress_images() {
  echo '----- Compressing images and populating premade -----'
  for image in resflash-${MACHINE}-*.img; do
    echo ${image}
    gzip -5 ${image}
    mv ${image}.gz premade/install
  done

  for image in resflash-${MACHINE}-*.fs; do
    echo ${image}
    gzip -5 ${image}
    mv ${image}.gz premade/upgrade
  done
}

# 1998 MB (not MiB, rounded to 1024)
echo '----- Building VGA image -----'
if [ ${MACHINE} == 'i386' ]; then
  ${RESFLASHBIN}/build_resflash.sh -n 1906 ${DESTDIR}
else
  ${RESFLASHBIN}/build_resflash.sh 1906 ${DESTDIR}
fi
compress_images

echo '----- Building com0 image -----'
if [ ${MACHINE} == 'i386' ]; then
  ${RESFLASHBIN}/build_resflash.sh -n -s ${COM0} 1906 ${DESTDIR}
else
  ${RESFLASHBIN}/build_resflash.sh -s ${COM0} 1906 ${DESTDIR}
fi
compress_images

echo '----- Calculating premade/SHA512 -----'
mv *.img.cksum premade/install
mv *.fs.cksum premade/upgrade

cd premade
find install/ upgrade/ -type f|sort|xargs cksum -a sha512 -h SHA512
cd ..

echo '----- Images complete! -----'
