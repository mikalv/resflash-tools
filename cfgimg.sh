#!/bin/sh

# Mount and configure resflash .img or .fs file
# Copyright Brian Conway <bconway@rcesoftware.com>, see LICENSE for details

set -o errexit
set -o nounset
if set -o|fgrep -q pipefail; then
  set -o pipefail
fi

if [ ${#} -ne 3 ]; then
  echo "Usage: ${0} resflash_dir resflash_img_or_fs short_hostname"
  exit 1
else
  RESFLASHBIN=${1}
  ORIGFILE=${2}
  CFGHOST=${3}
  IMGFILE=resflash-cfgimg-${CFGHOST}.$(echo ${ORIGFILE}|awk -F . -safe '{ print $NF }')
fi

# Source resflash.sub based on image location
. ${RESFLASHBIN}/resflash.sub
set_attr_by_machine $(machine)

echo '----- Cleaning old images -----'
rm -f resflash-cfgimg-*.{fs,img}*
sync

echo "----- Creating new image ${IMGFILE} -----"
cp -p ${ORIGFILE} ${IMGFILE}

# Assigns: MNTPATH
mount_img_or_fs ${IMGFILE}
df -h|fgrep ${MNTPATH}

# Common (/)

echo '----- Configuring / -----'

# Host-specific (/)

if [ -f ./cfgimg-root-${CFGHOST}.sh ]; then
  . ./cfgimg-root-${CFGHOST}.sh
fi

# Common (/var)

echo '----- Configuring /var -----'

# Host-specific (/var)

if [ -f ./cfgimg-var-${CFGHOST}.sh ]; then
  . ./cfgimg-var-${CFGHOST}.sh
fi

# Common (/etc)

echo '----- Configuring /etc -----'
sed -i '/^servers/s/pool/us.pool/' ${MNTPATH}/fs/etc/ntpd.conf

echo "${CFGHOST}.example.com" > ${MNTPATH}/fs/etc/myname

echo "127.0.0.1 localhost ${CFGHOST} ${CFGHOST}.example.com" > ${MNTPATH}/fs/etc/hosts
echo "::1 localhost ${CFGHOST} ${CFGHOST}.example.com" >> ${MNTPATH}/fs/etc/hosts

ln -sf /usr/share/zoneinfo/UTC ${MNTPATH}/fs/etc/localtime

echo 'ntpd_flags="-s"' > ${MNTPATH}/fs/etc/rc.conf.local
echo 'sndiod_flags=NO' >> ${MNTPATH}/fs/etc/rc.conf.local

echo 'lookup file bind' > ${MNTPATH}/fs/etc/resolv.conf.tail

# Host-specific (/etc)

if [ -f ./cfgimg-etc-${CFGHOST}.sh ]; then
  . ./cfgimg-etc-${CFGHOST}.sh
fi

# Common (/root)

echo '----- Configuring /root -----'

# Common (chroots)

echo '----- Running chroots -----'
chroot ${MNTPATH}/fs useradd -m -b /home -c "My User" myuser
chroot ${MNTPATH}/fs usermod -G wheel myuser
chmod 700 ${MNTPATH}/fs/home/myuser
echo myuser > ${MNTPATH}/fs/root/.forward

# Host-specific (chroots)

if [ -f ./cfgimg-chroot-${CFGHOST}.sh ]; then
  . ./cfgimg-chroot-${CFGHOST}.sh
fi

# Clean up
umount_all
rm -r ${MNTPATH}

echo '----- Calculating checksum -----'
cksum -a ${ALG} -h ${IMGFILE}.cksum ${IMGFILE}

echo '----- Configuration complete! -----'
