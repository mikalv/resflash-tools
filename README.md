# resflash-tools

### Sample tools for building and configuring resflash images

## Tools

- `build_premade.sh` - Script for building premade resflash images from -stable sets.
- `cfgimg.sh` - Script for mounting and configuring resflash .img or .fs files.
- `cfgimg-*-rtest.sh` - Sub-scripts specific to host `rtest`.
