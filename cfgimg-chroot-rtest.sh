#!/bin/sh

# Host-specific chroot configure for rtest
# Copyright Brian Conway <bconway@rcesoftware.com>, see LICENSE for details

chroot ${MNTPATH}/fs usermod -p '$2b$09$6SGJBUyfR.WEuS7VOy0gJ.2r0bdBt82mJ3.2THq508WcZXdl3Hp36' root
chroot ${MNTPATH}/fs usermod -p '$2b$08$Dnank9Rml4iDh68KXMHmY.OXJK1jCr84OrXMyw8wJWru/DV.Wi3tm' myuser
